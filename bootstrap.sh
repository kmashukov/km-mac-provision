#!/bin/bash

# TODO: prepare following roles
# profile-all: sketch abstract dropbox slack ?inbox? 
# konstantin – with spacemacs, goofy, microsoft-office, outlook, evernote, code
# profile-dev bash, git, 
# frontend
# designers

# TODO: write to cli and logs in the same time from this script

# TODO: rewrite welcome msg for script

#echo "[INFO] What you will get out of the box"
# TODO: If you are fine with this list of software "press any key for continue"
# If not, press ^C (Ctrl+C) and goto FILENAME and comment/uncomment needed software packages

# What if I will comment it in the early beginning? Will it be a problem?
# move to func: check that xcode is installed and its fresh
echo "[INFO] trying to setup xcode"
xcode-select --install

echo "[INFO] Installing homebrew"
# make idempotent and check that homebrew is already installed
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#echo "[INFO] Checking that configuration of this machine is suitable for homebrew"
##brew doctor

echo "[INFO] Updating brew packages database"
brew update

echo "[INFO] Installing python3 and ansible"
brew install python
brew install ansible
brew cask install caskroom/versions/java8

# run configutatin management with 
echo "[INFO] Running ansible playbook for Frontend Role"
ansible-playbook -i hosts -vv localhost.yml -K

echo "[INFO] Cleaning up (removing *.dmg files used for installation)"
brew cask cleanup
